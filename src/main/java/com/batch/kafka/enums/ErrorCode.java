package com.batch.kafka.enums;

public enum ErrorCode {
    // Global error
    UNKNOWN("ERR_UKN", "Something went wrong. Please try again"), INVALID_PARAMETERS("ERR_0001", "Invalid parameters"),
    INVALID_ACCESS_TOKEN("ERR_0002", "Invalid access token"), BAD_CREDENTIAL("ERR_0003", "Bad credentials"),
    // User error
    USER_NOT_FOUND("U_ERR_0001", "User not found"), USER_INACTIVE("U_ERR_0002", "User inactive"),
    USER_HAS_BEEN_ACTIVATED("U_ERR_0004", "User has been activated"), USER_EXISTED("U_ERR_0005", "user existed"),
    INVALID_PASSWORD("U_ERR_0006", "Invalid password"), INVALID_REFERRAL_CODE("U_ERR_0007", "Invalid referral code"),
    ROLE_NOT_FOUND("U_ERR_0008", "Role not found"), PERMISSION_NOT_FOUND("U_ERR_0009", "Permission not found"),
    USER_ALREADY_EXIST("U_ERR_0010", "User already exist"), EMAIL_ALREADY_EXIST("U_ERR_0011", "Email already exist"),
    // Verification error
    LIMIT_EXCEEDED("V_ERR_0001", "Exceed limitation"),
    VERIFICATION_CODE_NOT_FOUND("V_ERR_0002", "Verification code not found"),
    VERIFICATION_CODE_INVALID("V_ERR_0003", "Invalid verification code");

    private final String value;
    private final String reasonPhrase;

    ErrorCode(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    public String getCode() {
        return this.value;
    }

    public String getMessage() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return "ErrorCode{" + "value='" + value + '\'' + ", reasonPhrase='" + reasonPhrase + '\'' + '}';
    }
}

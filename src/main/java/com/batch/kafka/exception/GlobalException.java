package com.batch.kafka.exception;

import com.batch.kafka.enums.ErrorCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Setter
@Getter
public class GlobalException extends Exception {
	private String errorCode;
	private Optional data;

	public GlobalException(String message) {
		super(message);
	}

	public GlobalException(String message, String errorCode) {
		this(message);
		this.errorCode = errorCode;
		this.data = Optional.empty();
	}

	public GlobalException(ErrorCode error) {
		this(error.getMessage(), error.getCode());
	}

	public GlobalException(ErrorCode error, Optional data) {
		this(error);
		this.data = data;
	}

	public GlobalException(String message, String errorCode, String data) {
		this(message);
		this.errorCode = errorCode;
		this.data = Optional.of(data);
	}
}
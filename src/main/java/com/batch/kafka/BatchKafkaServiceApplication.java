package com.batch.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BatchKafkaServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchKafkaServiceApplication.class, args);
    }

}

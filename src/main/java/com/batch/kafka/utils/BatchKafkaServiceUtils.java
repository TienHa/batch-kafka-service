package com.batch.kafka.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.StringWriter;

public class BatchKafkaServiceUtils {
    /**
     * convert object to string
     *
     * @param object
     * @return
     * @throws IOException
     */
    public static String objectToString(Object object) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            StringWriter stringEmp = new StringWriter();
            objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            objectMapper.writeValue(stringEmp, object);
            return stringEmp.toString();
        } catch (Exception e) {
            return "";
        }

    }

    /**
     * convert string to object
     *
     * @param content
     * @param valueType
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T stringToObject(String content, Class<T> valueType) throws IOException {
        return new ObjectMapper().readValue(content, valueType);
    }
}

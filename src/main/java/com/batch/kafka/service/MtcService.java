package com.batch.kafka.service;

import com.batch.kafka.dto.MtcDto;

import java.util.List;

public interface MtcService {
    List<MtcDto> getAll();
}

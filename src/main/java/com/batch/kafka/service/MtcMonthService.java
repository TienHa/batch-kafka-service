package com.batch.kafka.service;

import com.batch.kafka.dto.MtcMonthDto;

import java.util.List;

public interface MtcMonthService {
    List<MtcMonthDto> getAll();
}

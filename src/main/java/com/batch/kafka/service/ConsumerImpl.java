package com.batch.kafka.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class ConsumerImpl {

    @KafkaListener(topics = "topic_test", groupId = "group-id")
    public void listen(String message) {
        System.out.println("Message" + message);
    }
}

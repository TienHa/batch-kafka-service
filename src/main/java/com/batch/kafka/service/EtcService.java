package com.batch.kafka.service;

import com.batch.kafka.dto.EtcDto;

import java.util.List;

public interface EtcService {
    List<EtcDto> getAll();
}

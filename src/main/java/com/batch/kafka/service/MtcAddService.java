package com.batch.kafka.service;

import com.batch.kafka.dto.MtcAddDto;

import java.util.List;

public interface MtcAddService {
    List<MtcAddDto> getAll();
}

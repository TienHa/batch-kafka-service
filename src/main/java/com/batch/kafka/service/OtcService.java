package com.batch.kafka.service;

import com.batch.kafka.dto.OtcDto;

import java.util.List;

public interface OtcService {
    List<OtcDto> getAll();
}

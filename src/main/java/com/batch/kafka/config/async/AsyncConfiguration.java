package com.batch.kafka.config.async;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class AsyncConfiguration {
    @Value("${thread-config.core-pool-size}")
    private Integer corePoolSize;

    @Value("${thread-config.max-pool-size}")
    private Integer maxPoolSize;

    @Bean(name = "ftRealTimeExecutor")
    public Executor ftRealTimeExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setThreadNamePrefix("FTRealTimeExecutor-");
        executor.initialize();
        return executor;
    }
}

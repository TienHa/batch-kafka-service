package com.batch.kafka.config.batch;

import com.batch.kafka.dto.*;
import com.batch.kafka.items.etc.EtcItemProcessor;
import com.batch.kafka.items.etc.EtcItemReader;
import com.batch.kafka.items.etc.EtcItemWriter;
import com.batch.kafka.items.mtc.MtcItemProcessor;
import com.batch.kafka.items.mtc.MtcItemReader;
import com.batch.kafka.items.mtc.MtcItemWriter;
import com.batch.kafka.items.mtcadd.MtcAddItemProcessor;
import com.batch.kafka.items.mtcadd.MtcAddItemReader;
import com.batch.kafka.items.mtcadd.MtcAddItemWriter;
import com.batch.kafka.items.mtcmonth.MtcMonthItemProcessor;
import com.batch.kafka.items.mtcmonth.MtcMonthItemReader;
import com.batch.kafka.items.mtcmonth.MtcMonthItemWriter;
import com.batch.kafka.items.otc.OtcItemProcessor;
import com.batch.kafka.items.otc.OtcItemReader;
import com.batch.kafka.items.otc.OtcItemWriter;
import com.batch.kafka.listener.*;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
public class BatchKafkaServiceConfiguration {
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private SpringBatchJobListener batchJobListener;
    @Autowired
    private MtcItemProcessor mtcItemProcessor;
    @Autowired
    private MtcItemReader mtcItemReader;
    @Autowired
    private MtcItemWriter mtcItemWriter;
    @Autowired
    private EtcItemProcessor etcItemProcessor;
    @Autowired
    private EtcItemReader etcItemReader;
    @Autowired
    private EtcItemWriter etcItemWriter;
    @Autowired
    private OtcItemProcessor otcItemProcessor;
    @Autowired
    private OtcItemReader otcItemReader;
    @Autowired
    private OtcItemWriter otcItemWriter;
    @Autowired
    private MtcAddItemProcessor mtcAddItemProcessor;
    @Autowired
    private MtcAddItemReader mtcAddItemReader;
    @Autowired
    private MtcAddItemWriter mtcAddItemWriter;
    @Autowired
    private MtcMonthItemProcessor mtcMonthItemProcessor;
    @Autowired
    private MtcMonthItemReader mtcMonthItemReader;
    @Autowired
    private MtcMonthItemWriter mtcMonthItemWriter;
    @Autowired
    private EtcListener etcListener;
    @Autowired
    private MtcListener mtcListener;
    @Autowired
    private OtcListener otcListener;
    @Autowired
    private MtcAddListener mtcAddListener;
    @Autowired
    private MtcMonthListener mtcMonthListener;

    public static final String BATCH_KAFKA_SERVICE = "batch_kafka_service";
    private static final String BATCH_KAFKA_SERVICE_ETC = "batch_kafka_service_etc";
    private static final String BATCH_KAFKA_SERVICE_MTC = "batch_kafka_service_mtc";
    private static final String BATCH_KAFKA_SERVICE_OTC = "batch_kafka_service_otc";
    private static final String BATCH_KAFKA_SERVICE_MTC_ADD = "batch_kafka_service_mtc_add";
    private static final String BATCH_KAFKA_SERVICE_MTC_MONTH = "batch_kafka_service_mtc_month";

    @Bean(name=BATCH_KAFKA_SERVICE)
    public Job batchKafkaService() {
        return jobBuilderFactory.get(BATCH_KAFKA_SERVICE)
                .listener(batchJobListener)
                .incrementer(new RunIdIncrementer())
                .start(splitFlow())
                .end()
                .build();
    }

    @Bean
    public Flow splitFlow() {
        final Flow etc = new FlowBuilder<Flow>(BATCH_KAFKA_SERVICE_ETC).from(batchKafkaServiceEtc()).end();
        final Flow mtc = new FlowBuilder<Flow>(BATCH_KAFKA_SERVICE_MTC).from(batchKafkaServiceMtc()).end();
        final Flow otc = new FlowBuilder<Flow>(BATCH_KAFKA_SERVICE_OTC).from(batchKafkaServiceOtc()).end();
        final Flow mtcAdd = new FlowBuilder<Flow>(BATCH_KAFKA_SERVICE_MTC_ADD).from(batchKafkaServiceMtcAdd()).end();
        final Flow mtcMonth = new FlowBuilder<Flow>(BATCH_KAFKA_SERVICE_MTC_MONTH).from(batchKafkaServiceMtcMonth()).end();


        return new FlowBuilder<Flow>("splitFlow")
                .split(new SimpleAsyncTaskExecutor())
                .add(etc, mtc, otc, mtcAdd, mtcMonth).build();
    }

    @Bean
    public Step batchKafkaServiceEtc() {
        return stepBuilderFactory.get(BATCH_KAFKA_SERVICE_ETC)
                .listener(etcListener)
                .<EtcDto, EtcDto>chunk(Integer.MAX_VALUE)
                .reader(etcItemReader)
                .processor(etcItemProcessor)
                .writer(etcItemWriter)
                .faultTolerant()
                .skip(Throwable.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

    @Bean
    public Step batchKafkaServiceMtc() {
        return stepBuilderFactory.get(BATCH_KAFKA_SERVICE_MTC)
                .listener(mtcListener)
                .<MtcDto, MtcDto>chunk(Integer.MAX_VALUE)
                .reader(mtcItemReader)
                .processor(mtcItemProcessor)
                .writer(mtcItemWriter)
                .faultTolerant()
                .skip(Throwable.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

    @Bean
    public Step batchKafkaServiceOtc() {
        return stepBuilderFactory.get(BATCH_KAFKA_SERVICE_OTC)
                .listener(otcListener)
                .<OtcDto, OtcDto>chunk(Integer.MAX_VALUE)
                .reader(otcItemReader)
                .processor(otcItemProcessor)
                .writer(otcItemWriter)
                .faultTolerant()
                .skip(Throwable.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

    @Bean
    public Step batchKafkaServiceMtcAdd() {
        return stepBuilderFactory.get(BATCH_KAFKA_SERVICE_MTC_ADD)
                .listener(mtcAddListener)
                .<MtcAddDto, MtcAddDto>chunk(Integer.MAX_VALUE)
                .reader(mtcAddItemReader)
                .processor(mtcAddItemProcessor)
                .writer(mtcAddItemWriter)
                .faultTolerant()
                .skip(Throwable.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }

    @Bean
    public Step batchKafkaServiceMtcMonth() {
        return stepBuilderFactory.get(BATCH_KAFKA_SERVICE_MTC_MONTH)
                .listener(mtcMonthListener)
                .<MtcMonthDto, MtcMonthDto>chunk(Integer.MAX_VALUE)
                .reader(mtcMonthItemReader)
                .processor(mtcMonthItemProcessor)
                .writer(mtcMonthItemWriter)
                .faultTolerant()
                .skip(Throwable.class)
                .skipLimit(Integer.MAX_VALUE)
                .build();
    }
}

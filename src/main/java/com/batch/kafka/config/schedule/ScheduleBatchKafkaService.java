package com.batch.kafka.config.schedule;

import com.batch.kafka.config.batch.BatchKafkaServiceConfiguration;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleBatchKafkaService {
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier(BatchKafkaServiceConfiguration.BATCH_KAFKA_SERVICE)
    private Job batchKafkaService;

    // start 6h:15 morning
    @Scheduled(cron = "0 15 6 ? * *")
    public String schedule() throws JobParametersInvalidException, JobExecutionAlreadyRunningException,
            JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(batchKafkaService, jobParameters);
        return "Batch job has been invoked";
    }
}

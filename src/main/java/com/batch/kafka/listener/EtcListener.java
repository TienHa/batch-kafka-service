package com.batch.kafka.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EtcListener implements StepExecutionListener {
    @Override
    public void beforeStep(StepExecution stepExecution) {
        log.info("Start Step ETC.");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        log.info("After Step ETC.", stepExecution.getExitStatus());
        return stepExecution.getExitStatus();
    }
}

package com.batch.kafka.items.otc;

import com.batch.kafka.dto.EtcDto;
import com.batch.kafka.dto.OtcDto;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@StepScope
@RequiredArgsConstructor
public class OtcItemProcessor implements ItemProcessor<OtcDto, OtcDto> {
    @Override
    public OtcDto process(OtcDto otcDto) throws Exception {
        return otcDto;
    }
}

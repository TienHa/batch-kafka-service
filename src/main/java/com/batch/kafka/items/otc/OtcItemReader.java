package com.batch.kafka.items.otc;

import com.batch.kafka.dto.EtcDto;
import com.batch.kafka.dto.MtcMonthDto;
import com.batch.kafka.dto.OtcDto;
import com.batch.kafka.items.AbstractItemReader;
import com.batch.kafka.service.EtcService;
import com.batch.kafka.service.OtcService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@StepScope
public class OtcItemReader extends AbstractItemReader<OtcDto> {
    @Autowired
    protected OtcService otcService;
    /**
     * Call data from service here
     * @return
     */
    @Override
    protected List<OtcDto> fetchRecords() {
        List<OtcDto> otcDtos = new ArrayList<>();
        otcDtos.add(new OtcDto("OTC"));
        return otcDtos;
    }
}

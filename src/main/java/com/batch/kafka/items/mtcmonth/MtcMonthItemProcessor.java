package com.batch.kafka.items.mtcmonth;

import com.batch.kafka.dto.MtcMonthDto;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@StepScope
@RequiredArgsConstructor
public class MtcMonthItemProcessor implements ItemProcessor<MtcMonthDto, MtcMonthDto> {
    @Override
    public MtcMonthDto process(MtcMonthDto mtcMonthDto) throws Exception {
        return mtcMonthDto;
    }
}

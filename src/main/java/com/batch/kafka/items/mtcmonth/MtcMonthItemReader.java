package com.batch.kafka.items.mtcmonth;

import com.batch.kafka.dto.MtcAddDto;
import com.batch.kafka.dto.MtcMonthDto;
import com.batch.kafka.items.AbstractItemReader;
import com.batch.kafka.service.MtcMonthService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@StepScope
public class MtcMonthItemReader extends AbstractItemReader<MtcMonthDto> {
    @Autowired
    protected MtcMonthService mtcMonthService;

    /**
     * Call data from service here
     * @return
     */
    @Override
    protected List<MtcMonthDto> fetchRecords() {
        List<MtcMonthDto> mtcMonthDtos = new ArrayList<>();
        mtcMonthDtos.add(new MtcMonthDto("MTC MONTH"));
        return mtcMonthDtos;
    }
}

package com.batch.kafka.items;

import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;

import java.util.Iterator;
import java.util.List;

public abstract class AbstractItemReader<T> extends AbstractItemCountingItemStreamItemReader<T> {

	private List<T> list;
	private Iterator<T> iterator;

	public AbstractItemReader() {
		super.setName(this.getClass().getSimpleName());
	}

	@Override
	public T doRead() {
		return iterator.hasNext() ? iterator.next() : null;
	}

	@Override
	public void doOpen() {
		list = fetchRecords();
		iterator = list.iterator();
	}

	@Override
	public void doClose() {
		list.clear();
	}

	abstract protected List<T> fetchRecords();

}

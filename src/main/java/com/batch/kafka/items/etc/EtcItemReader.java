package com.batch.kafka.items.etc;

import com.batch.kafka.dto.EtcDto;
import com.batch.kafka.items.AbstractItemReader;
import com.batch.kafka.service.EtcService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@StepScope
public class EtcItemReader extends AbstractItemReader<EtcDto> {
    @Autowired
    protected EtcService etcService;

    /**
     * Call data from service here
     * @return
     */
    @Override
    protected List<EtcDto> fetchRecords() {
        List<EtcDto> etcDtos = new ArrayList<>();
        etcDtos.add(new EtcDto("ETC"));
        return etcDtos;
    }
}

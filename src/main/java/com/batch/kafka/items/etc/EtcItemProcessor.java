package com.batch.kafka.items.etc;

import com.batch.kafka.dto.EtcDto;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@StepScope
@RequiredArgsConstructor
public class EtcItemProcessor implements ItemProcessor<EtcDto, EtcDto> {
    @Override
    public EtcDto process(EtcDto etcDto) throws Exception {
        return etcDto;
    }
}

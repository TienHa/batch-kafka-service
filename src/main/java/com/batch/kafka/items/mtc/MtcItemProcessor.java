package com.batch.kafka.items.mtc;

import com.batch.kafka.dto.MtcDto;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@StepScope
@RequiredArgsConstructor
public class MtcItemProcessor implements ItemProcessor<MtcDto, MtcDto> {
    @Override
    public MtcDto process(MtcDto mtcAppDto) throws Exception {
        return mtcAppDto;
    }
}

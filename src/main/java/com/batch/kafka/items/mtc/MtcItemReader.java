package com.batch.kafka.items.mtc;

import com.batch.kafka.dto.EtcDto;
import com.batch.kafka.dto.MtcDto;
import com.batch.kafka.items.AbstractItemReader;
import com.batch.kafka.service.MtcService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@StepScope
public class MtcItemReader extends AbstractItemReader<MtcDto> {
    @Autowired
    protected MtcService mtcService;

    /**
     * Call data from service here
     * @return
     */
    @Override
    protected List<MtcDto> fetchRecords() {
        List<MtcDto> mtcDtos = new ArrayList<>();
        mtcDtos.add(new MtcDto("MTC"));
        return mtcDtos;
    }
}

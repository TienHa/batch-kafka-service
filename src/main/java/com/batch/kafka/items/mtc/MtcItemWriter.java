package com.batch.kafka.items.mtc;

import com.batch.kafka.dto.EtcDto;
import com.batch.kafka.dto.MtcDto;
import com.batch.kafka.utils.BatchKafkaServiceUtils;
import lombok.NoArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@StepScope
@NoArgsConstructor
public class MtcItemWriter implements ItemWriter<MtcDto> {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Value("topic")
    private String topic;
    /**
     * send kafka here
     * @param list
     * @throws Exception
     */
    @Override
    public void write(List<? extends MtcDto> list) throws Exception {
        list.forEach(item -> {
            // example convert object to string
            kafkaTemplate.send("topic_test", BatchKafkaServiceUtils.objectToString(item));
        });
    }
}

package com.batch.kafka.items.mtcadd;

import com.batch.kafka.dto.MtcAddDto;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@StepScope
@RequiredArgsConstructor
public class MtcAddItemProcessor implements ItemProcessor<MtcAddDto, MtcAddDto> {
    @Override
    public MtcAddDto process(MtcAddDto mtcAddDto) throws Exception {
        return mtcAddDto;
    }
}

package com.batch.kafka.items.mtcadd;

import com.batch.kafka.dto.MtcAddDto;
import com.batch.kafka.dto.MtcDto;
import com.batch.kafka.items.AbstractItemReader;
import com.batch.kafka.service.MtcAddService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@StepScope
public class MtcAddItemReader extends AbstractItemReader<MtcAddDto> {
    @Autowired
    protected MtcAddService mtcAddService;

    /**
     * Call data from service here
     * @return
     */
    @Override
    protected List<MtcAddDto> fetchRecords() {
        List<MtcAddDto> mtcAddDtos = new ArrayList<>();
        mtcAddDtos.add(new MtcAddDto("MTC ADD"));
        return mtcAddDtos;
    }
}
